<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'fname_en' => 'Dara',
            'lname_en' => 'Chan',
            'username' => 'darachan',
//            'password' => bcrypt('123456'),
            'password' => '123456',
            'role_id' => 1,
            'email' => 'darachan@gmail.com',
            'phone' => '012789867',
            'gender' => 'f',
            'status' => 1,
            'address' => 'Phnom Penh, Cambodia',
            'create_at' => date('Y-m-d H:i:s'),
            'create_by' => 'Dara'
        ]);
    }
}