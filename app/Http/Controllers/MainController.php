<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    function index(){
        return redirect('/login');
    }

    function dashboard()
   	{
        $user = session()->get('user');
        if($user == ""){
            return redirect('/login');
        }else{
            return view('home');
        }
   	}
}
