<?php

namespace App\Http\Controllers;

use App\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\App;
use App\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if (session()->get('user') == null) {
            return redirect('/logout');
        }

        $roles = Role::getAllRole();
        return view("role.index", ['roles' => $roles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (session()->get('user') == null) {
            return redirect('/logout');
        }
        $permissions = Permission::getAllPermission();
        return view("role.create", ["permissions" => $permissions]);
    }

    /*Function add user role name and role permission include Description*/
    public function addUserRole($menuID, $userRole)
    {

        $arrSplitRole = explode(",", $userRole);

        if ($arrSplitRole[0] == '') {
            return redirect('admin/role/create')->with('message', 'Role name is required');
        }

        $name = $arrSplitRole[0];
        $description = $arrSplitRole[1];
        $status = $arrSplitRole[2];
        $created_at = date('Y-m-d H:i:s');
        $updated_at = date('0-0-0 0:0:0');
        $created_by = session()->get('user_id');

        $data = array("role_name" => $name, "description" => $description, "status" => $status,
            "create_at" => $created_at, "update_at" => $updated_at, "create_by" => $created_by);

        DB::table("role")->insert($data);

        $last_role_id = Role::select('Id_role')->max('Id_role');
        $strMenu = $menuID;
        $arrSplitMenu = explode(",", $strMenu);
        for ($i = 0; $i < count($arrSplitMenu); $i++) {
            $data = array("role_id" => $last_role_id, "permission_id" => $arrSplitMenu[$i]);

            DB::table("role_permission")->insert($data);
        }
    }
    
    /*Function add user role name and Description without permission is selected*/
    public function addOnlyRoleDesc($userRole)
    {

        $arrSplitRole = explode(",", $userRole);

        $name = $arrSplitRole[0];
        $description = $arrSplitRole[1];
        $status = $arrSplitRole[2];
        $created_at = date('Y-m-d H:i:s');
        $updated_at = date('0-0-0 0:0:0');
        $created_by = session()->get('user_id');

        $data = array("role_name" => $name, "description" => $description, "status" => $status,
            "create_at" => $created_at, "update_at" => $updated_at, "create_by" => $created_by);

        DB::table("role")->insert($data);

    }

    /*Function Edit user role name and role permission include Description*/
    public function updateUserRole($menuID, $roleData)
    {

        $arrSplitPermis = explode(",", $menuID);
        $arrSplitRole = explode(",", $roleData);

        $role_id = $arrSplitRole[0];
        $name = $arrSplitRole[1];
        $description = $arrSplitRole[2];
        $status = $arrSplitRole[3];;
        $updated_at = date('Y-m-d H:i:s');
        $updated_by = session()->get('user_id');

        Role::where('Id_role', $arrSplitRole[0])
            ->update(
                array(
                    'role_name' => $name,
                    'description' => $description,
                    'status' => $status,
                    'update_at' => $updated_at,
                    'update_by' => $updated_by
                )
            );

        Role::removeRolePermis($role_id);

        $strMenu = $menuID;
        $arrSplitMenu = explode(",", $strMenu);
        for ($i = 0; $i < count($arrSplitMenu); $i++) {
            $data = array("role_id" => $arrSplitRole[0], "permission_id" => $arrSplitMenu[$i]);

            DB::table("role_permission")->insert($data);
        }
    }

    /*Function Edit user role name and role permission only*/
    public function updateOnlyRoleDesc($roleData)
    {

        $arrSplitRole = explode(",", $roleData);

        $role_id = $arrSplitRole[0];
        $name = $arrSplitRole[1];
        $description = $arrSplitRole[2];
        $status = $arrSplitRole[3];
        $updated_at = date('Y-m-d H:i:s');
        $updated_by = session()->get('user_id');

        Role::where('Id_role', $arrSplitRole[0])
            ->update(
                array(
                    'role_name' => $name,
                    'description' => $description,
                    'status' => $status,
                    'update_at' => $updated_at,
                    'update_by' => $updated_by
                )
            );

       Role::removeRolePermis($role_id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Permission $permission
     * @return \Illuminate\Http\Response
     */
    public function editRole($role_id)
    {
        if (session()->get('user') == null) {
            return redirect('/logout');
        }
        $permissions = Permission::getAllPermission();
        $role_data = Role::getEditData($role_id);
        $role_perm_data = Role::getRolePermissionData($role_id);
        $only_one_role_perm = Role::getOnlyOneRolePermData($role_id);
        $rolePermission = "";
        $rolePerms = "";

        /* If no permission is set to Role */
        if (count($role_perm_data) == 0) {
            return view("role.edit", ["permissions" => $permissions, "role" => $role_data,
                "role_perm_only_one" => null, "role_permissions" => null]);
        } else {
            /* If has only one permission is set to Role */
            if (count($role_perm_data) == 1) {

                return view("role.edit", ["permissions" => $permissions, "role" => $role_data,
                    "role_perm_only_one" => $only_one_role_perm, "role_permissions" => null]);

            } else {
                /* If has permissions two up are set to Role */
                foreach ($role_perm_data as $role_permis) {

                    $rolePermission .= $role_permis->permission_id . ',';
                    $rolePerms = rtrim($rolePermission, ',');
                    /*RolePermission can be string like this "12345678" */
                }

                return view("role.edit", ["permissions" => $permissions, "role" => $role_data,
                    "role_perm_only_one" => null, "role_permissions" => $rolePerms]);

            }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Permission $permission
     * @return \Illuminate\Http\Response
     */

    public function delete($role_id)
    {
        if (session()->get('user') == null) {
            return redirect('/logout');
        }
          Role::where('Id_role', $role_id)
            ->update(
                array('status' => 0)
            );

        Role::removeRolePermis($role_id);

        return redirect('admin/role')->with('message', 'Role permission has been deleted!');
    }
}
