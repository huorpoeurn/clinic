<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Session\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Permission;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     *
     */

     /*  Function for login user authentication  */

    public function index(){
        $user = session()->get('user');
        if($user == ""){
           return view('login');
        }else{
            return redirect('/admin');
        }
    }

    public function doLogin(){
        $username = Input::get("username");
        $password= Input::get("password");

        $users = DB::table('user')->get();

        $result = ""; $user_id = "";
        foreach($users as $user) {
            if($username == $user->username && $password == $user->password){
                $result = $user->username;
                $user_id = $user->Id_user;
            }
        }

         session()->put('user',$result);
         session()->put('user_id',$user_id);

        /*Get all Menu*/
        $permissions = Permission::getOnlyPermName();
        session()->put('permissions',$permissions);

        if($result != ""){
            return redirect('/admin');
        }else{
            return view('login');
        }
    }

    /*  Function for logout user authentication  */

    public static function doLogout(){
         Auth::logout();
         session()->flush();
        return redirect('/login');
    }
}
