<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\App;
use App\Permission;
use App\Role;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        if (session()->get('user') == null) {
            return redirect('/logout');
        }
//        $permissions = Permission::getAllPermission();
//        return view("user/index", ['permissions' => $permissions]);
        return view("user/index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (session()->get('user') == null) {
            return redirect('/logout');
        }
        return view('user/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->input("name_en");
        $username = $request->input("username");
        $role = $request->input("role");
        $password = $request->input("password");
        $re_password = $request->input("re_password");

        $session_error = "";
        $field_name = "";
//       if($name == "" || $username == "" || $role == "" || $password == "" || $re_password == "") {
//           if ($name == "") {
//               $session_error .= "eng_name"; $field_name .= "Name EN";
//           } elseif ($username == "") {
//               $session_error .= "username"; $field_name .= "Username";
//           } elseif ($role == "") {
//               $session_error .= "role"; $field_name .= "Role";
//           } elseif ($password == "") {
//               $session_error .= "password"; $field_name .= "Password";
//           } elseif ($re_password == "") {
//               $session_error .= "re_password"; $field_name .= "Retype password";
//           }
//           return redirect('admin/user/create')->with($session_error, $field_name.' '. 'is required!');
//       }

//        $description = $request->input("description");
//        $status = $request->input("status");
//        $created_at = date('Y-m-d H:i:s');
//        $updated_at = date('0-0-0 0:0:0');
//        $created_by = session()->get('user_id');
//
//        $data = array("permission_name" => $name, "description" => $description, "status" => $status
//        , "create_at" => $created_at, "update_at" => $updated_at, "create_by" => $created_by);
//
//        DB::table("permission")->insert($data);
//
//        return redirect('admin/permission')->with('message', 'Permission has been added successful!');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Permission $permission
     * @return \Illuminate\Http\Response
     */
    public function show($permiss_id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Permission $permission
     * @return \Illuminate\Http\Response
     */
    public function edit($permiss_id)
    {
        if (session()->get('user') == null) {
            return redirect('/logout');
        }
        $data = Permission::getEditData($permiss_id);
        return view("permission/edit", ['permiss' => $data]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Permission $permission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (session()->get('user') == null) {
            return redirect('/logout');
        }
        $name = $request->input("permission");
        $description = $request->input("description");
        $status = $request->input("status");
        $updated_at = date('Y-m-d H:i:s');
        $updated_by = session()->get('user_id');

        if ($name == '') {
            return redirect()->back()->with('message', 'permission name is required');
        }

        $IsUpdate = Permission::where('Id_permission', $id)
            ->update(
                array(
                    'permission_name' => $name,
                    'description' => $description,
                    'status' => $status,
                    'update_at' => $updated_at,
                    'update_by' => $updated_by
                )
            );

        if ($IsUpdate) {
            return redirect('admin/permission')->with('message', 'Permission has been updated!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Permission $permission
     * @return \Illuminate\Http\Response
     */

    public function delete($permiss_id)
    {
        if (session()->get('user') == null) {
            return redirect('/logout');
        }
        $IsDelete = Permission::where('Id_permission', $permiss_id)
            ->update(
                array('status' => 0)
            );

        if ($IsDelete) {
            return redirect('admin/permission')->with('message', 'Permission has been deleted!');
        }
    }
}
