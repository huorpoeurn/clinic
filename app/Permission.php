<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Permission extends Model
{
    protected $fillable = [
        'Id_permission',  'permission_name', 'description', 'status','create_at','update_at','create_by', 'update_by'
    ];

    protected $table = 'permission';
    protected $primaryKey = 'Id_permission';
    public $timestamps = false;

    public static function getAllPermission()
    {
       return DB::table('permission')
            ->select('Id_permission', 'permission_name','description')
            ->where('status',1)
            ->orderby('Id_permission','DESC')
            ->get();
    }

    public static function getOnlyPermName()
    {
        return DB::table('permission')
            ->select('permission_name')
            ->where('status',1)
            ->get();
    }

    public static function getEditData($permiss_id)
    {
        return DB::table('permission')
            ->select('Id_permission', 'permission_name','status','description')
            ->where('Id_permission', $permiss_id)
            ->first();
    }

}
