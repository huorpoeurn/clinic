<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Role extends Model
{
    protected $fillable = [
        'Id_role',  'role_name', 'description', 'status','create_at','update_at','create_by', 'update_by'
    ];

    protected $table = 'role';
    protected $primaryKey = 'Id_role';
    public $timestamps = false;

    public static function getAllRole()
    {
        return DB::table('role')
            ->select('Id_role', 'role_name','description','status')
            ->orderby('Id_role','DESC')
            ->get();
    }

    public static function getEditData($role_id)
    {
        return DB::table('role')
            ->select('Id_role', 'role_name','status','description')
            ->where('Id_role', $role_id)
            ->first();
    }

    public static function getRolePermissionData($role_id)
    {
        return DB::table('role_permission')
            ->select('role_id', 'permission_id')
            ->where('role_id', $role_id)
            ->get();
    }

    public static function getOnlyOneRolePermData($role_id)
    {
        return DB::table('role_permission')
            ->select('role_id', 'permission_id')
            ->where('role_id', $role_id)
            ->first();
    }

    public static function removeRolePermis($role_id){
        return DB::table('role_permission')
            ->where('role_id', $role_id)
            ->delete();
    }
}
