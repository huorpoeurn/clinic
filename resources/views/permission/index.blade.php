@extends('layouts.dashboardlayout')
@section('content')

    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
                    <h4 class="permission-label">{{ Request::segment(2) }}</h4>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-4">
                    <h4 class="permission-label"><a href="{{ url('admin/permission') }}" class="list-show">List</a></h4>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-4">
                    <h4 class="permission-label"><a href="{{ url('admin/permission/create') }}"
                                                    class="add-label">Add</a></h4>
                </div>
                <div>
                    @if(session()->has('message'))
                        <div class="alert alert-success message-success" id="message_success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <p style="border: 1px solid #E3EBED; margin: 16px 0;"></p>
                </div>
            </div>

            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <table class="table-bordered" id="dataTable" width="100%">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Permission</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($permissions as $key => $permiss)
                            <tr>
                                <td>{{ $key+=1 }}</td>
                                <td> {{ $permiss->permission_name }}</td>
                                <td> {{ $permiss->description }}</td>
                                <td>
                                    <a href="permission/{{$permiss->Id_permission}}/edit">
                                        <span style="color:#27367F;" class="mdi mdi-24px mdi-pencil-box-outline"></span>
                                    </a>
                                    <a class="delete_icon" title="Delete" href="#" data-data-id="{{$permiss->Id_permission}}"
                                     data-data-segment="{{ Request::segment(2) }}">
                                        <span style="color:#D9534F;" class="mdi mdi-24px mdi-delete-forever"></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>

            </div>

        </div>
    </div>

@endsection()


