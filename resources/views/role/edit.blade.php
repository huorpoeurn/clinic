@extends('layouts.dashboardlayout')
@section('content')

    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
                    <h4 class="permission-label">{{ Request::segment(2) }}</h4>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-4">
                    <h4 class="permission-label"><a href="{{ url('admin/role') }}" class="list-label">List</a>
                    </h4>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-4">
                    <h4 class="permission-label"><a href="{{ url('admin/permission/create') }}" class="add-show">Add</a>
                    </h4>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <p style="border: 1px solid #E3EBED; margin: 16px 0;"></p>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4"></div>
                <!-- block of role -->
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
                    <form action="#" method="GET">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group row">
                                    <label for="role_name" class="col-sm-2 col-form-label">Name
                                        <span class="required-color">*</span></label>
                                    <div class="col-sm-10">

                                        <input type="hidden" class="form-control" id="role_id"
                                               name="role_id" value="{{ $role->Id_role }}">

                                        <input type="text" class="form-control" id="role_name"
                                               name="role" value="{{ $role->role_name }}" placeholder="Role Name">

                                        <span class="message-error" id="message_error"></span>
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <label for="description" class="col-sm-2 col-form-label">Description</label>
                                    <div class="col-sm-10">
                                <textarea class="form-control" id="description" name="description"
                                          rows="7">{{ $role->description }}</textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="description" class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                        <select class="form-control select-option" name="status" id="status">
                                            @if( $role->status == 1)
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            @else
                                                <option value="0">Inactive</option>
                                                <option value="1">Active</option>
                                            @endif

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-2 col-md-2 col-sm-2 col-form-label"></label>
                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                        <button type="submit" class="btn btn-sm btn-primary" id="btnUpdate">Update
                                        </button>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                        <a href="{{url('admin/role')}}">
                                            <button type="button" class="btn btn-sm btn-danger">Cancel</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- End block of role -->

                            <!-- block of Permission -->
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <h4 class="select-permission">Select Permission</h4>
                                <div class="vl"></div>

                                @foreach($permissions as $key => $permiss)

                                    <label class="checkbox-inline chMenu" style="margin: 0px 50px;
                            padding: 10px 38px; white-space: nowrap;">

                                    @if($role_perm_only_one != null)
                                        <!--If already set permission to user access for only one role permission-->
                                            @if($role_perm_only_one->permission_id == $permiss->Id_permission)
                                                <input type="checkbox" value="{{$permiss->Id_permission}}"
                                                       name="permissions[]"
                                                       checked>
                                            @else
                                            <!--If not yet set permission to user access for one or all role permission-->
                                                <input type="checkbox" value="{{$permiss->Id_permission}}"
                                                       name="permissions[]">
                                            @endif

                                        @endif

                                    <!--If already set permission to user access for many role permission-->
                                        @if($role_permissions != null)

                                            <?php
                                            $role_perms_data = explode(",", $role_permissions);
                                            ?>

                                            @foreach($role_perms_data as $key => $data)
                                                @if($role_perms_data[$key] == $permiss->Id_permission)
                                                    <input type="checkbox"
                                                           value={{$permiss->Id_permission}} name="permissions[]"
                                                           checked="checked"/>
                                                @endif
                                            @endforeach

                                            <input type="checkbox"
                                                   value={{$permiss->Id_permission}} name="permissions[]"/>
                                        @else
                                        <!--If no permission is set to user access-->
                                            @if(count($role_perm_only_one) == 0)
                                                <input type="checkbox"
                                                       value={{$permiss->Id_permission}} name="permissions[]"/>
                                            @endif

                                        @endif

                                    <!--Show checkbox and all list permission -->
                                        <span class="checkmark"></span>
                                        {{ $permiss->permission_name }}

                                    </label>

                                @endforeach

                            </div>
                        </div>

                    </form>
                    <!-- End block of Permission -->
                </div>
            </div>
        </div>
    </div>

@endsection()
