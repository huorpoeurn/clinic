@extends('layouts.dashboardlayout')
@section('content')

    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
                    <h4 class="permission-label">{{ Request::segment(2) }}</h4>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-4">
                    <h4 class="permission-label"><a href="{{ url('admin/role') }}" class="list-show">List</a></h4>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-4">
                    <h4 class="permission-label"><a href="{{ url('admin/role/create') }}"
                                                    class="add-label">Add</a></h4>
                </div>
                <div>
                    @if(session()->has('message'))
                        <div class="alert alert-success message-success" id="message_success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <p style="border: 1px solid #E3EBED; margin: 16px 0;"></p>
                </div>
            </div>

            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <table class="table-bordered" id="dataTable" width="100%">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Role Name</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($roles as $key => $role)
                            <tr>
                                <td>{{ $key+=1 }}</td>
                                <td> {{ $role->role_name }}</td>
                                <td> {{ $role->description }}</td>
                                <td>
                                    @if($role->status == 1)
                                        Active
                                        @else
                                        Inactive
                                        @endif
                                </td>
                                <td>
                                    <a href="role/editRole/{{$role->Id_role}}">
                                        <span style="color:#27367F;" class="mdi mdi-24px mdi-pencil-box-outline"></span>
                                    </a>
                                    <a class="delete_icon" title="Delete" href="#" data-data-id="{{$role->Id_role}}"
                                       data-data-segment="{{ Request::segment(2) }}">
                                        <span style="color:#D9534F;" class="mdi mdi-24px mdi-delete-forever"></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>

            </div>

        </div>
    </div>

@endsection()


