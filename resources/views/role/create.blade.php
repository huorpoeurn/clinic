@extends('layouts.dashboardlayout')
@section('content')

    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
                    <h4 class="permission-label">{{ Request::segment(2) }}</h4>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-4">
                    <h4 class="permission-label"><a href="{{ url('admin/role') }}" class="list-label">List</a>
                    </h4>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-4">
                    <h4 class="permission-label"><a href="{{ url('admin/permission/create') }}" class="add-show">Add</a>
                    </h4>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <p style="border: 1px solid #E3EBED; margin: 16px 0;"></p>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4"></div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-8">

                    <form class="pt-3" action="#" method="GET">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group row">
                            <label for="role_name" class="col-sm-2 col-form-label">Name <span
                                        class="required-color">*</span></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="role_name"
                                       name="role" placeholder="Role Name">

                                <span class="message-error" id="message_error"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-sm-2 col-form-label">Description</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" id="description" name="description" rows="7"></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="description" class="col-sm-2 col-form-label">Status</label>
                            <div class="col-sm-10">
                                <select class="form-control select-option" name="status" id="status">
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-2 col-md-2 col-sm-2 col-form-label"></label>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <button type="submit" class="btn btn-sm btn-primary" id="btnAdd">Save</button>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <a href="{{url('admin/role')}}">
                                    <button type="button" class="btn btn-sm btn-danger">Cancel</button>
                                </a>
                            </div>
                        </div>

                    </form>

                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <h4 class="select-permission">Select Permission</h4>
                    <div class="vl"></div>

                    @foreach($permissions as $key => $permiss)

                        <label class="checkbox-inline chMenu" style="margin: 0px 50px;
                            padding: 10px 38px; white-space: nowrap;">
                            <input type="checkbox" value="{{$permiss->Id_permission}}" name="permissions[]">
                            <span class="checkmark"></span>
                            {{ $permiss->permission_name }}
                        </label>

                    @endforeach

                </div>

            </div>
        </div>
    </div>

@endsection()
