<!-- partial -->
<div class="container-fluid page-body-wrapper">
    <!-- partial:partials/_sidebar.html -->
    <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">

            @if(session()->get('permissions') != null)

                @foreach(session()->get('permissions') as $permiss)

                    @if($permiss->permission_name == "Home")

                        <li class="{{Request::segment(2) == '' ? 'menu-bg' : ''}} nav-item">
                            <a class="nav-link" href="{{url('admin')}}">
                                <i class="mdi mdi-home menu-icon"
                                   style="{{Request::segment(2) == '' ? 'color: #ffffff;' : 'color: #000000;'}}"></i>
                                <span class="{{Request::segment(2) == '' ? 'menu-text' : ''}}">{{$permiss->permission_name}}</span>
                            </a>
                        </li>

                    @endif

                    @if($permiss->permission_name == "Permission")

                        <li class="{{Request::segment(2) == 'permission' ? 'menu-bg' : ''}} nav-item">
                            <a class="nav-link" href="{{ url('admin/permission') }}" aria-expanded="false"
                               aria-controls="ui-basic">
                                <i class="mdi mdi-format-line-spacing menu-icon"
                                   style="{{Request::segment(2) == 'permission' ? 'color: #ffffff;' : 'color: #000000;'}}"></i>
                                <span class="{{Request::segment(2) == 'permission' ? 'menu-text' : ''}}">{{$permiss->permission_name}}</span>
                            </a>
                        </li>

                    @endif

                    @if($permiss->permission_name == "User")
                        <li class="{{Request::segment(2) == 'user' ? 'menu-bg' : ''}} nav-item">
                            <a class="nav-link" href="{{ url('admin/user') }}" aria-expanded="false"
                               aria-controls="ui-basic">
                                <i class="mdi mdi-account-multiple-plus menu-icon"
                                   style="{{Request::segment(2) == 'user' ? 'color: #ffffff;' : 'color: #000000;'}}"></i>
                                <span class="{{Request::segment(2) == 'user' ? 'menu-text' : ''}}">{{$permiss->permission_name}}</span>
                            </a>
                        </li>
                    @endif

                    @if($permiss->permission_name == "Category")
                        <li class="{{Request::segment(2) == 'category' ? 'menu-bg' : ''}} nav-item">
                            <a class="nav-link" href="{{ url('admin/category')}}" aria-expanded="false"
                               aria-controls="ui-basic">
                                <i class="mdi mdi-view-headline menu-icon"
                                   style="{{Request::segment(2) == 'category' ? 'color: #ffffff;' : 'color: #000000;'}}"></i>
                                <span class="{{Request::segment(2) == 'category' ? 'menu-text' : ''}}">{{$permiss->permission_name}}</span>
                            </a>
                        </li>
                    @endif

                    @if($permiss->permission_name == "Medical")
                        <li class="{{Request::segment(2) == 'medical' ? 'menu-bg' : ''}} nav-item">
                            <a class="nav-link" href="{{ url('admin/medical') }}" aria-expanded="false"
                               aria-controls="ui-basic">
                                <i class="mdi mdi-grid-large menu-icon"
                                   style="{{Request::segment(2) == 'medical' ? 'color: #ffffff;' : 'color: #000000;'}}"></i>
                                <span class="{{Request::segment(2) == 'medical' ? 'menu-text' : ''}}">{{$permiss->permission_name}}</span>
                            </a>
                        </li>
                    @endif

                    @if($permiss->permission_name == "Invoice")
                        <li class="{{Request::segment(2) == 'invoice' ? 'menu-bg' : ''}} nav-item">
                            <a class="nav-link" href="{{ url('admin/medical') }}" aria-expanded="false"
                               aria-controls="ui-basic">
                                <i class="mdi mdi-coin menu-icon"
                                   style="{{Request::segment(2) == 'invoice' ? 'color: #ffffff;' : 'color: #000000;'}}"></i>
                                <span class="{{Request::segment(2) == 'invoice' ? 'menu-text' : ''}}">{{$permiss->permission_name}}</span>
                            </a>
                        </li>
                    @endif

                    @if($permiss->permission_name == "Patient")
                        <li class="{{Request::segment(2) == 'patient' ? 'menu-bg' : ''}} nav-item">
                            <a class="nav-link" href="{{ url('admin/patient') }}" aria-expanded="false"
                               aria-controls="ui-basic">
                                <i class="mdi mdi-account-multiple menu-icon"
                                   style="{{Request::segment(2) == 'patient' ? 'color: #ffffff;' : 'color: #000000;'}}"></i>
                                <span class="{{Request::segment(2) == 'patient' ? 'menu-text' : ''}}">{{$permiss->permission_name}}</span>
                            </a>
                        </li>
                    @endif

                    @if($permiss->permission_name == "Staff")
                        <li class="{{Request::segment(2) == 'staff' ? 'menu-bg' : ''}} nav-item">
                            <a class="nav-link" href="{{ url('admin/staff') }}" aria-expanded="false"
                               aria-controls="ui-basic">
                                <i class="mdi mdi-account-multiple-outline menu-icon"
                                   style="{{Request::segment(2) == 'staff' ? 'color: #ffffff;' : 'color: #000000;'}}"></i>
                                <span class="{{Request::segment(2) == 'staff' ? 'menu-text' : ''}}">{{$permiss->permission_name}}</span>
                            </a>
                        </li>
                    @endif

                    @if($permiss->permission_name == "Appointment")
                        <li class="{{Request::segment(2) == 'appointment' ? 'menu-bg' : ''}} nav-item">
                            <a class="nav-link" href="{{ url('admin/appointment') }}" aria-expanded="false"
                               aria-controls="ui-basic">
                                <i class="mdi mdi-bell-ring-outline menu-icon"
                                   style="{{Request::segment(2) == 'appointment' ? 'color: #ffffff;' : 'color: #000000;'}}"></i>
                                <span class="{{Request::segment(2) == 'appointment' ? 'menu-text' : ''}}">{{$permiss->permission_name}}</span>
                            </a>
                        </li>
                    @endif

                    @if($permiss->permission_name == "Treatement")
                        <li class="{{Request::segment(2) == 'appointment' ? 'menu-bg' : ''}} nav-item">
                            <a class="nav-link" href="{{ url('admin/treatement') }}" aria-expanded="false"
                               aria-controls="ui-basic">
                                <i class="mdi mdi-chart-line menu-icon"
                                   style="{{Request::segment(2) == 'treatement' ? 'color: #ffffff;' : 'color: #000000;'}}"></i>
                                <span class="{{Request::segment(2) == 'treatement' ? 'menu-text' : ''}}">{{$permiss->permission_name}}</span>
                            </a>
                        </li>
                    @endif

                        @if($permiss->permission_name == "Role")

                            <li class="{{Request::segment(2) == 'role' ? 'menu-bg' : ''}} nav-item">
                                <a class="nav-link" href="{{ url('admin/role') }}" aria-expanded="false"
                                   aria-controls="ui-basic">
                                    <i class="mdi mdi-account-key menu-icon"
                                       style="{{Request::segment(2) == 'role' ? 'color: #ffffff;' : 'color: #000000;'}}"></i>
                                    <span class="{{Request::segment(2) == 'role' ? 'menu-text' : ''}}">{{$permiss->permission_name}}</span>
                                </a>
                            </li>

                        @endif

                @endforeach
            @endif

            {{--<li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false"
                   aria-controls="ui-basic">
                    <i class="mdi mdi-account-multiple-plus menu-icon"></i>
                    <span class="menu-title">Users</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse" id="ui-basic">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item">
                            <a class="nav-link" href="pages/ui-features/buttons.html">All Users</a>
                        </li>
                        <!-- <li class="nav-item"> <a class="nav-link" href="pages/ui-features/typography.html">Typography</a></li> -->
                    </ul>
                </div>
            </li>--}}

        </ul>
    </nav>