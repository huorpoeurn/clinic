<!-- partial:partials/_footer.html -->

        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <script src="{{asset('css/vendors/base/vendor.bundle.base.js')}}"></script>
  <!-- endinject -->

  <!-- Plugin js for this page-->
  <script src="{{asset('css/vendors/chart.js/Chart.min.js')}}"></script>
  <script src="{{asset('css/vendors/datatables.net/jquery.dataTables.js')}}"></script>
{{--  <script src="{{asset('css/vendors/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>--}}
  <!-- End plugin js for this page-->

  <!-- inject:js -->
  <script src="{{asset('js/off-canvas.js')}}"></script>
  <script src="{{asset('js/hoverable-collapse.js')}}"></script>
  <script src="{{asset('js/template.js')}}"></script>
  <!-- endinject -->

  <!-- Custom js for this page-->
  <script src="{{asset('js/dashboard.js')}}"></script>
  <script src="{{asset('js/sweetalert/sweetalert.min.js')}}"></script>
  <script src="{{asset('js/dataTables.bootstrap4.js')}}"></script>
  <script src="{{asset('js/work-script.js')}}"></script>
  <!-- End custom js for this page-->

</body>

</html>