<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Clinic</title>
  <link rel="stylesheet" type="text/css" href="{{asset('css/vendors/mdi/css/materialdesignicons.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/vendors/base/vendor.bundle.base.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/vendors/datatables.net-bs4/dataTables.bootstrap4.css')}}">

  <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/cus_style.css')}}">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/favicon.png" />
</head>
<body>
	@include('templates.header')
	@include('templates.sidebar')
	@yield('content')
	@include('templates.footer')
