@extends('layouts.dashboardlayout')
@section('content')

    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
                    <h4 class="permission-label">{{ Request::segment(2) }}</h4>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-4">
                    <h4 class="permission-label"><a href="{{ url('admin/user') }}" class="list-label">List</a>
                    </h4>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-4">
                    <h4 class="permission-label"><a href="{{ url('admin/user/create') }}" class="add-show">Add</a>
                    </h4>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <p style="border: 1px solid #E3EBED; margin: 16px 0;"></p>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12"></div>
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">

                    <form class="pt-3" action="{{ url('/admin/user') }}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group row">
                                    <label for="name_en" class="col-sm-2 col-form-label text_nowrap">Name EN <span
                                                class="required-color">*</span>
                                    </label>
                                    <div class="col-sm-10 clearable-input">
                                        <input type="text" class="form-control" id="name_en" name="name_en"
                                               placeholder="Full Name EN">
                                        <span data-clear-input>&times;</span>
                                        <span class="message-error">
                                            @if(session()->has('eng_name'))
                                                <div class="error-text" id="message_error">
                                                    {{ session()->get('eng_name') }}
                                                </div>
                                            @endif
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name_kh" class="col-sm-2 col-form-label">Name KH </label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control clearable" id="name_kh" name="name_kh"
                                               placeholder="Full Name KH">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="username" class="col-sm-2 col-form-label text_nowrap">Username <span
                                                class="required-color">*</span>
                                    </label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="username" name="username"
                                               placeholder="Username">

                                        <span class="message-error">
                                            @if(session()->has('username'))
                                                <div class="error-text" id="message_error">
                                                    {{ session()->get('username') }}
                                                </div>
                                            @endif
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="role" class="col-sm-2 col-form-label text_nowrap">Role
                                        <span class="required-color">*</span>
                                    </label>
                                    <div class="col-sm-10">
                                        <select class="form-control select-option" name="role" id="role">
                                            <option value="0">-- Select Role --</option>
                                            <option value="1">Admin</option>
                                            <option value="2">Doctor</option>
                                        </select>

                                        <span class="message-error">
                                            @if(session()->has('role'))
                                                <div class="error-text" id="message_error">
                                                    {{ session()->get('role') }}
                                                </div>
                                            @endif
                                        </span>
                                    </div>
                            </span>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="col-sm-2 col-form-label text_nowrap">Password <span
                                                class="required-color">*</span>
                                    </label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="password" name="password"
                                               placeholder="Password">

                                        <span class="message-error">
                                            @if(session()->has('password'))
                                                <div class="error-text" id="message_error">
                                                    {{ session()->get('password') }}
                                                </div>
                                            @endif
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="re_password" class="col-sm-2 col-form-label text_nowrap">Re-Pass<span
                                                class="required-color"> *</span>
                                    </label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="re_password" name="re_password"
                                               placeholder="Retype password">

                                        <span class="message-error">
                                            @if(session()->has('re_password'))
                                                <div class="error-text" id="message_error">
                                                    {{ session()->get('re_password') }}
                                                </div>
                                            @endif
                                        </span>
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                                <div class="form-group row">
                                    <label for="phone" class="col-sm-2 col-form-label">Phone </label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="phone" name="phone"
                                               placeholder="Phone">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-sm-2 col-form-label">Email </label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="email" name="email"
                                               placeholder="Email">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="description" class="col-sm-2 col-form-label">Description</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" id="description" name="description"
                                                  rows="7"></textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="description" class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                        <select class="form-control select-option" name="status">
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-2 col-md-2 col-sm-2 col-form-label"></label>
                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                        <button type="submit" class="btn btn-sm btn-primary" id="btnUserSave">Save</button>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                        <a href="{{url('admin/user')}}">
                                            <button type="button" class="btn btn-sm btn-danger">Cancel</button>
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>

@endsection()
