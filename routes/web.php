<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','MainController@index');
Route::get('/login','Auth\LoginController@index');
Route::get('/logout', 'Auth\LoginController@doLogout');

Route::group(['prefix' => 'admin'], function () {

    Route::post('/', 'Auth\LoginController@doLogin');
    Route::get('/','MainController@dashboard');

    Route::resource('/permission','PermissionController');
    Route::get('/permission/delete/{id}','PermissionController@delete');

    Route::resource('/role','RoleController');
    Route::get('/role/editRole/{roleID}','RoleController@editRole');
    // Add Role and permission for user access
    Route::get('/role/addUserRole/{menuID}/{userRole}','RoleController@addUserRole');
    // Add only Role for user access
    Route::get('/role/addOnlyRoleDesc/{userRole}','RoleController@addOnlyRoleDesc');
    //Update Role and permission for user access
    Route::get('/role/editRole/updateUserRole/{menuID}/{userRole}','RoleController@updateUserRole');
    //Update only Role for user access
    Route::get('/role/editRole/updateOnlyRoleDesc/{userRole}','RoleController@updateOnlyRoleDesc');
    Route::get('/role/delete/{id}','RoleController@delete');

    Route::resource('/user','UserController');
});

