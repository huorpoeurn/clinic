jQuery(document).ready(function ($) {

    //Function DataTable
    $.noConflict();
    $('#dataTable').DataTable({
        "pagingType": "simple_numbers"
    });

    //Function Delete popup
    $(".delete_icon").click(function (event) {
        event.preventDefault(); // prevent form submit
        //get user-id when click on button delete
        var id = $(this).data('data-id');
         var segment = $(this).data('data-segment');
        swal({
            title: "Are you sure?",
            text: "Once deleted, it will be gone away",
            icon: "warning",
            buttons: true,
            dangerMode: true
        })
            .then(
                function (isConfirm) {
                    if (isConfirm) {
                        location = segment + "/delete/" + id
                    }
                }
            );
    });


    setTimeout(function () {
        $("#message_success").fadeOut("slow");
    }, 3000);

    $("#permission_name").keyup(function () {
        var permission_name = document.getElementById("permission_name").value;

        if (permission_name != "") {
            //  alert("Yes");
            $('#message_error').hide();
        } else {
            $('#message_error').show();
        }
    });

    $("#role_name").keyup(function () {
        var role_name = document.getElementById("role_name").value;
        if (role_name != "") {
            //  alert("Yes");
            $('#message_error').hide();
        } else {
            $('#message_error').show();
        }
    });

    //Function Add Role and permission
    $("#btnAdd").click(function () {
        var roleName = $("#role_name").val();
        var roleDesc = $("#description").val();
        var e = document.getElementById("status");
        var status = e.options[e.selectedIndex].value;
        var checkMenu = $('input[name="permissions[]"]:checked').map(function () {
            return $(this).val();
        }).get();

        if (checkMenu == "" && roleName == "") {
            document.getElementById("message_error").innerHTML = 'User role is required!';
            return false;
        }

        if (checkMenu == "") {
            event.preventDefault(); // prevent form submit
            /*Function add user role name and Description without permission are selected*/
            $.ajax({
                type: 'GET',
                url: 'addOnlyRoleDesc/' + roleName + "," + roleDesc + "," + status,
                data: {userRole: roleName + "," + roleDesc + "," + status},
                success: function (data) {
                }
            });

            swal({
                title: "Good job!",
                text: "User Role has been saved successfully",
                icon: "success",
                buttons: false,
                timer: 3000,
            });

        } else {

            if (roleName === "") {
                event.preventDefault(); // prevent form submit
                document.getElementById("message_error").innerHTML = 'User role is required!';
                return false;

            } else {
                /*Function add user role name and role permission include Description*/
                $.ajax({
                    type: 'GET',
                    url: 'addUserRole/' + checkMenu + "/" + roleName + "," + roleDesc + "," + status,
                    data: {menuID: checkMenu, userRole: roleName + "," + roleDesc + "," + status},
                    success: function (data) {
                    }
                });
                event.preventDefault(); // prevent form submit
                swal({
                    title: "Good job!",
                    text: "User Role has been saved successfully",
                    icon: "success",
                    buttons: false,
                    timer: 3000,
                });
            }
        }

    });
    //End of Function Add Role and permission

    //Function update Role and permission
    $("#btnUpdate").click(function () {
        var roleID = $("#role_id").val();
        var roleName = $("#role_name").val();
        var roleDesc = $("#description").val();
        var e = document.getElementById("status");
        var status = e.options[e.selectedIndex].value;
        var checkMenu = $('input[name="permissions[]"]:checked').map(function () {
            return $(this).val();
        }).get();

        if (checkMenu == "" && roleName == "") {
            document.getElementById("message_error").innerHTML = 'User role is required!';
            return false;
        }

        if (checkMenu == "") {
            event.preventDefault(); // prevent form submit
            /*Function Update only user role name and Description without permission is select*/
            $.ajax({
                type: 'GET',
                url: 'updateOnlyRoleDesc/' + roleID + "," + roleName + "," + roleDesc + "," + status,
                data: {userRole: roleID + "," + roleName + "," + roleDesc + "," + status},
                success: function (data) {
                }
            });

            swal({
                title: "Good job!",
                text: "User Role has been saved successfully",
                icon: "success",
                buttons: false,
                timer: 3000,
            });

        } else {

            if (roleName === "") {
                event.preventDefault(); // prevent form submit

                document.getElementById("message_error").innerHTML = 'User role is required!';
                return false;

            } else {
                /*Function add user role name and role permission include Description*/
                $.ajax({
                    type: 'GET',
                    url: 'updateUserRole/' + checkMenu + "/" + roleID + "," + roleName + "," + roleDesc + "," + status,
                    data: {menuID: checkMenu, userRole: +roleID + "," + roleName + "," + roleDesc + "," + status},
                    success: function (data) {
                    }
                });
                event.preventDefault(); // prevent form submit
                swal({
                    title: "Good job!",
                    text: "User Role has been saved successfully",
                    icon: "success",
                    buttons: false,
                    timer: 3000,
                });
            }
        }
    });
    //End of Function Update Role and permission

   /* New on 10-July-2019 */

    /**
     * Clearable text inputs
     */
    // function tog(v){return v?'addClass':'removeClass';}
    // $(document).on('input', '.clearable', function(){
    //     $(this)[tog(this.value)]('x');
    // }).on('mousemove', '.x', function( e ){
    //     $(this)[tog(this.offsetWidth-18 < e.clientX-this.getBoundingClientRect().left)]('onX');
    // }).on('touchstart click', '.onX', function( ev ){
    //     ev.preventDefault();
    //     $(this).removeClass('x onX').val('').change();
    // });

    Array.prototype.forEach.call(document.querySelectorAll('.clearable-input>[data-clear-input]'), function(el) {
        el.addEventListener('click', function(e) {
            e.target.previousElementSibling.value = '';
        });
    });

    $("#btnUserSave").on("click", function () {
        event.preventDefault(); // prevent form submit

        var name = document.getElementById("name_en").value;

        alert(name);


        // if($name == "" || $username == "" || $role == "" || $password == "" || $re_password == "") {
        //     if ($name == "") {
        //         $session_error .= "eng_name"; $field_name .= "Name EN";
        //     } elseif ($username == "") {
        //         $session_error .= "username"; $field_name .= "Username";
        //     } elseif ($role == "") {
        //         $session_error .= "role"; $field_name .= "Role";
        //     } elseif ($password == "") {
        //         $session_error .= "password"; $field_name .= "Password";
        //     } elseif ($re_password == "") {
        //         $session_error .= "re_password"; $field_name .= "Retype password";
        //     }
        //     return redirect('admin/user/create')->with($session_error, $field_name.' '. 'is required!');
        // }
    });

});